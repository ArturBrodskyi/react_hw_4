import { useState, useEffect } from "react";
import { useImmer } from "use-immer";
import axios from "axios";
import ItemList from "./components/itemList";
import "./App.scss";
import Counter from "./components/Cart&FavCounter";
import Modal from "./components/Modal";
import Router from "./Router.jsx";
import Header from "./components/Header";
import { useDispatch, useSelector } from "react-redux";
import { fetchItems, addInCart, deleteFromCart, favouriteFunction } from "./components/redux/storeSlice.js";
import { openModalSlice, closeModalSlice, openDeleteModalSlice, closeDeleteModalSlice  } from "./components/redux/modalSlice.js";


function App() {

  const items = useSelector(state => state.store.items)
  const cart = useSelector(state => state.store.cart)
  const favourites = useSelector(state => state.store.favourites)
  const dispatch = useDispatch()
  const modalState = useSelector(state => state.modal)
  const selectedItem = modalState.selectedItem;
  const isModalOpen = modalState.isModalOpen
  const isDeleteModalOpen = modalState.isDeleteModalOpen


  useEffect(() => {
    dispatch(fetchItems());
  }, []);
  
  const openModal = (item) => {
    dispatch(openModalSlice(item))
  };

  const closeModal = (item) => {
    dispatch(closeModalSlice(item))
  };

  const openDeleteModal = (item) => {
    dispatch(openDeleteModalSlice(item))
  }

  const closeDeleteModal = (item) => {
    dispatch(closeDeleteModalSlice(item))
  }

  const addToCart = (item) => {
      const cartItem = items.find(({ articul }) => articul === item.articul);
      dispatch(addInCart(cartItem))
      
    closeModal();
  };
  const removeFromCart = (item) => {
    dispatch(deleteFromCart(item.articul))
    closeDeleteModal()
  };
    const cartNumber = cart.length;
    const favNumber = favourites.length;

  const FavouriteFn = (item) => {
    dispatch(favouriteFunction(item))
  };
  return (
    <>
      <Header></Header>
      <Router
        items={items}
        openModal={openModal}
        addToCart={addToCart}
        cartNumber={cartNumber}
        favNumber={favNumber}
        cart={cart}
        removeFromCart={removeFromCart}
        openDeleteModal={openDeleteModal}
        FavouriteFn={FavouriteFn}

      ></Router>

      {selectedItem && (
        <Modal
          headerCont="Add to cart?"
          bodyCont={`Add ${selectedItem.name} to cart?`}
          firstButtonText="Add to cart"
          addToCart={() => addToCart(selectedItem)}
          className="fav-modal"
          isModalOpen={isModalOpen}
          onCloseClick={closeModal}
        ></Modal>
      )}
      {selectedItem && (
      <Modal
        headerCont="Remove from cart?"
        bodyCont={`Remove ${selectedItem.name} from cart?`}
        firstButtonText="Ok"
        removeFromCart={() => removeFromCart(selectedItem)}
        className="fav-modal"
        isDeleteModalOpen={isDeleteModalOpen}
        closeDeleteModal={closeDeleteModal}
      ></Modal>
    )}
    </>
  );
}
export default App;
