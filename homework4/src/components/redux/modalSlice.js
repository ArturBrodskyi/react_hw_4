import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isModalOpen: false,
  isDeleteModalOpen: false,
  selectedItem: '',
};

const modalSlice = createSlice({
  name: "modal",
  initialState,
  reducers: {
    openModalSlice(state, action) {
      state.isModalOpen = true;
      state.selectedItem = action.payload
    },
    closeModalSlice(state, action){
        state.isModalOpen = false;
        state.selectedItem = action.payload
    },
    openDeleteModalSlice(state, action){
        state.isDeleteModalOpen = true;
        state.selectedItem = action.payload
    },
    closeDeleteModalSlice(state, action){
        state.isModalOpen = false;
        state.selectedItem = action.payload
    }
  },
});

export const { openModalSlice, closeModalSlice, closeDeleteModalSlice , openDeleteModalSlice} = modalSlice.actions;
export default modalSlice.reducer;
